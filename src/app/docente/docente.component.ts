import { Component, OnInit, Output,EventEmitter } from '@angular/core';
import { Docente } from './Modelo/Docente';

@Component({
  selector: 'app-docente',
  templateUrl: './docente.component.html',
  styleUrls: ['./docente.component.css']
})
export class DocenteComponent implements OnInit {

  @Output() docenteAgregado=new EventEmitter<Docente>();

  constructor() {

  }

  ngOnInit() {
  }

  Enviar(nombre,area,puesto){
    this.docenteAgregado.emit(new Docente(nombre.value,area.value,puesto.value,0));
  }
}
