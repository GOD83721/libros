import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes,RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { DocenteComponent } from './docente/docente.component';
import { DocenteListaComponent } from './docente-lista/docente-lista.component';
import { FilaDocenteComponent } from './fila-docente/fila-docente.component';
import { HttpClientModule } from '@angular/common/http';
import { LibroComponent } from './libro/libro.component';
import { PrestamoComponent } from './prestamo/prestamo.component';
import { MenuComponent } from './menu/menu.component';

const appRoutes:Routes=[
  {path:'',redirectTo:'/libro',pathMatch:'full'},
  {path: 'libro', component:LibroComponent},
  {path: 'prestamo', component:PrestamoComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    DocenteComponent,
    DocenteListaComponent,
    FilaDocenteComponent,
    LibroComponent,
    PrestamoComponent,
    MenuComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
