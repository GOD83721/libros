import { Component, OnInit } from '@angular/core';
import {Docente} from './../docente/Modelo/Docente';
import { HttpClient, HttpParams } from '@angular/common/http';

@Component({
  selector: 'app-docente-lista',
  templateUrl: './docente-lista.component.html',
  styleUrls: ['./docente-lista.component.css']
})
export class DocenteListaComponent implements OnInit {
  docentes: Docente[];

  constructor(private http: HttpClient) {
    this.docentes=[];
   }

  ngOnInit() {
    this.http.get("http://localhost/practica/docentes")
      .subscribe(
        res=>
        this.docentes=res as Docente[]
        //console.log(res);
      );
  }
  
  AddDocente(docente){
    this.docentes.unshift(docente);
  }
}
