import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilaDocenteComponent } from './fila-docente.component';

describe('FilaDocenteComponent', () => {
  let component: FilaDocenteComponent;
  let fixture: ComponentFixture<FilaDocenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilaDocenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilaDocenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
