import { Component, OnInit, Input } from '@angular/core';
import { Docente } from './../docente/Modelo/Docente';

@Component({
  selector: 'app-fila-docente',
  templateUrl: './fila-docente.component.html',
  styleUrls: ['./fila-docente.component.css']
})
export class FilaDocenteComponent implements OnInit {

  public nombre:string;
  public puesto:string;
  public area:string;
  
  @Input("docente") data:Docente;

  constructor() { }

  ngOnInit() {
    this.nombre=this.data.nombre;
    this.puesto=this.data.puesto;
    this.area=this.data.area;
  }

}
